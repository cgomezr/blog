# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Blog::Application.config.secret_key_base = 'b3c4a73c3e374b44f718ed91557198e827758177eba4ac761bbc858ca51eaaddbffa33016a0c65dd8bbc6f174856bb26de15a66eef4c241e3dacf6aac71cf2be'
